# Informações

Por enquanto só tem um spider single page da zapgrafica. Para automatizar para outras páginas da zapgrafica será rápido!

O processo será semelhante para as outras páginas.

# Pré-requisitos

- Ter o Scrapy instalado: `sudo pip install scrapy`

# Execução

- Rodar o comando: `scrapy crawl zapgrafica`

# Output

- A execução do spider zapgrafica irá gerar um arquivo json, `abada.json`. Abada foi a página que usei como teste.

### Obs: Bug que não deu tempo de resolver

- Na página da zapgráfica, os dados estavam vindo com códigos ascii, optei por ignorá-los. O trecho de código que faz isso é: `encode("ascii", "ignore")`. Sem isso alguns dados podem parecer estranhos. Por exemplo palavras que possuem acentuação.
