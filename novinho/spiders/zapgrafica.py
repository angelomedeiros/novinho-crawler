import scrapy
import json


class ZapgraficaSpider(scrapy.Spider):
    name = "zapgrafica"

    def start_requests(self):
        yield scrapy.Request(url="https://www.zapgrafica.com.br/loja/home/categoria/abada", callback=self.parse_produto)

    def parse_produto(self, response):
        arr = []

        for card in response.css(".caption.deslogado"):
            title = card.css("h5::text").extract_first()
            info = card.css(".box-info-servico")
            ref = info.css(
                "p:nth-child(1)::text").extract_first()
            sizeOfArt = info.css(
                "p:nth-child(2)::text").extract_first()
            sizeEnd = info.css(
                "p:nth-child(3)::text").extract_first()
            printing = info.css(
                "p:nth-child(4)::text").extract_first()
            deadline = info.css(
                "p:nth-child(5) > span > strong::text").extract_first()

            arr.append({
                "title": title.encode("ascii", "ignore").replace("\n", ""),
                "ref": ref.encode("ascii", "ignore"),
                "sizeOfArt": sizeOfArt.encode("ascii", "ignore"),
                "sizeEnd": sizeEnd.encode("ascii", "ignore"),
                "printing": printing.encode("ascii", "ignore"),
                "deadline": deadline.encode("ascii", "ignore"),
            })

        with open("abada.json", "wb") as f:
            json.dump(arr, f)
